@extends('layouts.app')

@section('title','Create candidate')

@section('content')

<div class="content">
<div class="title m-b-md">

<h1>Create candidates </h1>
<form method = "post" action = "{{action('CandidatesController@store')}}">
@csrf <!--אבטחת מידע-->

<div class="form-group">
    <label for = "name">Candidates name </label>
    <input type = "text" name = "name">
</div>
<div class="form-group" >
    <label for = "email">Candidates email </label>
    <input type = "text" class="fotm-control" name = "email">
</div>
<div>
    <input type="submit" class="fotm-control" name="submit" value = "Create candidate">
</div>
@yield('content') 
</form>
@endsection     
