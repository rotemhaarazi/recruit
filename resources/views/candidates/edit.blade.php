@extends('layouts.app')
@section('title','Edit candidate')
@section('content')
<div class="content">
<div class="title m-b-md">

    <h1>Edit candidates </h1>
    <form method = "post" action = "{{action('CandidatesController@update',$candidate->id)}}">
    @method('PATCH')
    @csrf <!--אבטחת מידע-->
    <div class="form-group">
        <label for = "name">Candidates name </label>
        <input type = "text" class="fotm-control" name = "name" value = {{$candidate->name}}>
    </div>
    <div class="form-group">
        <label for = "email">Candidates email </label>
        <input type = "text" class="fotm-control" name = "email" value = {{$candidate->email}}>
    </div>
    <div>
        <input type = "submit" name = "submit" value = "Edit candidate">
    </div>
</form>
@endsection     