<?php

use Illuminate\Support\Facades\Route; //יוס עושה אינקלוד מתוחכם

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome'); 
});

Route::resource('candidates', 'CandidatesController')->middleware('auth');

Route::get('candidates/changeuser/{cid}/{uid?}','CandidatesController@changeUser')->name('candidate.changeuser');

Route::get('candidates/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidates.changestatus')->middleware('auth');

Route::get('candidates/delete/{id}','CandidatesController@destroy')->name('candidate.delete');



Route::get('/hello',function(){
    return 'Hello Laravel';

});

route::get('/students/{id}',function($id='No students'){
    return 'We got students with id '.$id;//שרשור משתנים בתוך סטרינג

});

route::get('/car/{id?}',function($id=null){//דיפולטיבי חייב לבוא עם סימן שאלה
    if(isset($id)){ //פונקציה שבודקת האם קיים ערך משתנה כלשהו
        //TODO: valide for integer וולידציה לוודא שהמתשנה הוא מספר או סטרינג
        return "we got car $id" ;
    }else{
        return 'we need id to find your car';   //גרשיים כפולים - שרשור משתנים בתוך סטרינג

    }
    
   //גרשיים כפולים -v שרשור משתנים בתוך סטרינג

});

Route::get('/comment/{id}', function ($id) {
    return view('comment', compact('id')); 
});

route::get('/users/{email}/{name?}',function($email=null ,$name=null){//דיפולטיביי חייב לבוא עם סימן שאלה
    return view('users',compact('name','email')); 
    }
);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
